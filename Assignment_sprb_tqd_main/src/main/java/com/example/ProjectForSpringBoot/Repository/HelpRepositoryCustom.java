package com.example.ProjectForSpringBoot.Repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.example.ProjectForSpringBoot.Model.Help;

public interface HelpRepositoryCustom {
	
	public List<Help> getLstHelp(Integer pageNo,Integer pageSize,String staffId,Integer parentHelpId,String status,String type,Integer shop_id);
}
