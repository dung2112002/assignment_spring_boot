package com.example.ProjectForSpringBoot.Repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.example.ProjectForSpringBoot.Model.Help;
import com.example.ProjectForSpringBoot.Model.Staff;
@Repository
public class StaffRepositoryCustomIplm implements StaffRepositoryCustom{

	@PersistenceContext
	EntityManager em;
	@Override
	public List<Staff> getStaffList(Integer pageNo, Integer pageSize,String staffCode, String staffName, String tel, String status, Integer shopId,
			Integer helpId, String idNo) throws Exception {
		if(status.length()<1) {
			throw new Exception("status is invalid");
		}
		StringBuilder sql = new StringBuilder("select distinct s.* from Help h join Staff s on h.created_user = s.staff_code "
				+ "where s.status =:status ");
		if(staffCode!=null) {
			sql.append("and s.staff_code=:staffCode ");	
		}
		if(staffName!=null) {
			sql.append("and s.staff_name like :staffNameLow or  s.staff_name like:staffNameUpC");
		}
		if(tel!=null) {
			sql.append("and s.tel=:tel ");
		}
		if(shopId!=null) {
			sql.append("and s.shop_id=:shopId ");
		}
		if(helpId!=null) {
			sql.append("and h.help_id=:helpId ");
		}
		if(idNo!=null) {
			sql.append("and s.id_no=:idNo ");
		}
		sql.append("order by s.staff_id asc ");
		Query query = em.createNativeQuery(sql.toString());
		query.setParameter("status", status);
		if(staffCode!=null) {
			query.setParameter("staffCode", staffCode);
		}
		if(staffName!=null) {
			query.setParameter("staffNameLow","%"+staffName.toLowerCase()+"%");
			query.setParameter("staffNameUpC","%"+staffName.toUpperCase()+"%");
		}
		if(tel!=null) {
			query.setParameter("tel", tel);
		}
		if(shopId!=null) {
			query.setParameter("shopId", shopId);
		}
		if(helpId!=null) {
			query.setParameter("helpId", helpId);
		}
		if(idNo!=null) {
			query.setParameter("idNo", idNo);
		}
		List<Object[]> objLst= query.setFirstResult(pageNo).setMaxResults(pageSize).getResultList();	
		List<Staff> staffLst= new ArrayList<>();
		for (Object[] obj : objLst) {
			Staff staff = new Staff();
			staff.setStaffId(Integer.parseInt(obj[0].toString().trim()));
			if(obj[1]!=null)staff.setStaffCode(obj[1].toString());
			if(obj[2]!=null)staff.setStaffName((String)obj[2].toString());
			if(obj[3]!=null)staff.setTel(obj[3].toString());
			if(obj[4]!=null)staff.setAddress(obj[4].toString());
			if(obj[5]!=null)staff.setShopId(Integer.parseInt(obj[5].toString().trim()));
			if(obj[6]!=null)staff.setId_no(obj[6].toString());
			if(obj[7]!=null)staff.setStatus(obj[7].toString());
			staffLst.add(staff);
		}
		return staffLst;
	}

}
