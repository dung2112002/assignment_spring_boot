package com.example.ProjectForSpringBoot.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.example.ProjectForSpringBoot.CustomException.BusinessException;
import com.example.ProjectForSpringBoot.Model.Help;
@Repository
public class HelpRepositoryIplm implements HelpRepositoryCustom{

	@PersistenceContext
	EntityManager em;
	
	@Override
	public List<Help> getLstHelp(Integer pageNo,Integer pageSize,String staffIdRaw, Integer parentHelpId, String status, String type, Integer shopId) {
		if(staffIdRaw.length()<1) {
			throw new BusinessException("staffId is invalid");
		}
		Integer staffId;
		List<Help> helpLst= new ArrayList<>();
		try {
			 staffId=Integer.parseInt(staffIdRaw);
		} catch (NumberFormatException e) {
			throw new BusinessException("staffId must be a number");
		}		
		try {
			
			StringBuilder sql = new StringBuilder("select h.* from Help h join Staff s on h.created_user = s.staff_code "
					+ "where s.staff_id =:staffId ");
			if(parentHelpId!=null) {
				sql.append("and h.parent_help_id =:parentHelpId ");
			}
			if(status!=null) {
				sql.append("and h.status =:status ");
			}
			if(type!=null) {
				sql.append("and h.type =:type ");
			}
			if(shopId!=null) {
				sql.append("and s.shop_id =:shopId");
			}
			Query query = em.createNativeQuery(sql.toString());
			query.setParameter("staffId", staffId);
			if(parentHelpId!=null) {
				query.setParameter("parentHelpId", parentHelpId);
			}
			if(status!=null) {
				query.setParameter("status", status);
			}
			if(type!=null) {
				query.setParameter("type", type);
			}
			if(shopId!=null) {
				query.setParameter("shopId", shopId);
			}
			
			List<Object[]> objLst= query.setFirstResult(pageNo).setMaxResults(pageSize).getResultList();			
			for (Object[] obj : objLst) {
				Help help = new Help();
				help.setHelpId(Integer.parseInt(obj[0].toString().trim()));
				if(obj[1]!=null)help.setHelpName(obj[1].toString());
				if(obj[2]!=null)help.setContent(obj[5].toString());
				if(obj[3]!=null)help.setCreatedUser(obj[7].toString());
				if(obj[4]!=null)help.setParentHelpId(Integer.parseInt(obj[2].toString().trim()));
				if(obj[5]!=null)help.setType(obj[3].toString());
				if(obj[6]!=null)help.setPosition(obj[4].toString());
				if(obj[7]!=null)help.setStatus(obj[6].toString());
				helpLst.add(help);
			}
			
			
		} catch (Exception e) {
			throw new BusinessException("Something went wrong in HelpRepositoryIplm!");
		}
		return helpLst;
	}

	
	
}
