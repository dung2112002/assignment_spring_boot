package com.example.ProjectForSpringBoot.Repository;
import java.util.List;

import com.example.ProjectForSpringBoot.Model.Staff;
public interface StaffRepositoryCustom {
	public List<Staff> getStaffList(Integer pageNo, Integer pageSize,String staffCode,String staffName,String tel,String Status,Integer shopId,Integer helpId,String idNo) throws Exception;	
	
}
