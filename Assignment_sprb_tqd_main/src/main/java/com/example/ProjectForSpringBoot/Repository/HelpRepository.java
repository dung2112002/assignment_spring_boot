package com.example.ProjectForSpringBoot.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.ProjectForSpringBoot.Model.Help;
@Repository
public interface HelpRepository extends JpaRepository<Help,Integer>{
}
