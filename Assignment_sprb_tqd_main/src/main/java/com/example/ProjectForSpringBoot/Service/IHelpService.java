package com.example.ProjectForSpringBoot.Service;

import java.util.List;

import javax.transaction.Transactional;

import com.example.ProjectForSpringBoot.Model.Help;

public interface IHelpService {
	//delete help
	@Transactional
	public void deleteHelp(Integer helpId);
	//insert help
	public Help insertHelp(Help help);
	//update help
	@Transactional
	public Help updateHelp(String helpId,Help help);
	//get list help
	public List<Help> getHelpLst(Integer pageNo,Integer pageSize);
	//get list help by search
	public List<Help> getHelpLstBySearch(int pageNo,int PageSize,String staffId, Integer parentHelpId, String status, String type, Integer shopId);
}
