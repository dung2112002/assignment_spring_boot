package com.example.ProjectForSpringBoot.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ProjectForSpringBoot.Model.Staff;
import com.example.ProjectForSpringBoot.Repository.StaffRepositoryCustom;
@Service
public class StaffServiceIplm implements IStaffService {
	@Autowired
	StaffRepositoryCustom staffRepoCus;
	@Override
	public List<Staff> getStaffLst(Integer pageNo, Integer pageSize,String staffCode, String staffName, String tel, String status, Integer shopId,
			Integer helpId, String idNo) throws Exception {
		// TODO Auto-generated method stub
		return staffRepoCus.getStaffList(pageNo,pageSize,staffCode, staffName, tel, status, shopId, helpId, idNo);
	}
	
}
