package com.example.ProjectForSpringBoot.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.ProjectForSpringBoot.CustomException.BusinessException;
import com.example.ProjectForSpringBoot.Model.Help;
import com.example.ProjectForSpringBoot.Repository.HelpRepository;
import com.example.ProjectForSpringBoot.Repository.HelpRepositoryCustom;
@Service
public class HelpServiceIplm implements IHelpService {
	@Autowired
	private HelpRepository helpRepo;
	@Autowired
	private HelpRepositoryCustom helpRepoCus;

	@Override
	public List<Help> getHelpLst(Integer pageNo,Integer pageSize) {
		Pageable paging = PageRequest.of(pageNo, pageSize);
		Page<Help> PageResult = helpRepo.findAll(paging);
		
	  return PageResult.toList();
	}
	@Override
	public void deleteHelp(Integer helpId) {
		boolean isHelpExist= helpRepo.existsById(helpId);
		if(isHelpExist==false) {
			throw new BusinessException("helpId does not exist");
		}
		try {
			Help helpUpdate= helpRepo.getById(helpId);				
					helpUpdate.setStatus("0");
					helpRepo.save(helpUpdate);			
			
		} catch (Exception e) {
			throw new BusinessException("Something in HelpService went wrong!");
		}

		
	}

	@Override
	public Help insertHelp(Help help) {
		if(help.getHelpName()==null
				||help.getParentHelpId()==null
				||help.getCreatedUser()==null
				||help.getStatus()==null
				||help.getCreatedUser()==null) {
			throw new BusinessException("HelpName,ParentHelpId,CreatedUser,Status,"
					+ "CreatedUser must not be empty");
		}
		try {
				return helpRepo.save(help);
		}
		catch (Exception e) {
			throw new BusinessException("Something in HelpService went wrong!");
		}
	}

	@Override
	public Help updateHelp(String helpIdRaw, Help help) {
		Integer helpId;
		try {
			 helpId = Integer.parseInt(helpIdRaw);
		} catch (NumberFormatException e) {
			throw new BusinessException("helpId must be a number");
		}
		boolean isHelpExist= helpRepo.existsById(helpId);
		if(isHelpExist==false) {
			throw new BusinessException("helpId does not exist");
		}
		try {
				Help helpUpdate= helpRepo.getById(helpId);
			     	if(help.getHelpId()!=null)helpUpdate.setHelpId(help.getParentHelpId());
					if(help.getHelpName()!=null)helpUpdate.setHelpName(help.getHelpName());
					if(help.getContent()!=null)helpUpdate.setContent(help.getContent());
					if(help.getPosition()!=null)helpUpdate.setPosition(help.getPosition());
					if(help.getCreatedUser()!=null)helpUpdate.setCreatedUser(help.getCreatedUser());
					if(help.getStatus()!=null)helpUpdate.setStatus(help.getStatus());
					if(help.getType()!=null)helpUpdate.setType(help.getType());
					return helpRepo.save(helpUpdate);
			
		} catch (Exception e) {
			throw new BusinessException("Something in HelpService went wrong!");
		}
		
	}

	@Override
	public List<Help> getHelpLstBySearch(int pageNo,int pageSize,String staffId, 
			Integer parentHelpId, String status, String type, Integer shopId){		
		return helpRepoCus.getLstHelp(pageNo,pageSize,staffId,parentHelpId, status, type, shopId);
	}
	

	
}
