package com.example.ProjectForSpringBoot.Service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.ProjectForSpringBoot.Model.Staff;

public interface IStaffService {
	public List<Staff> getStaffLst( Integer pageNo, Integer pageSize,String staffCode, String staffName, String tel, String status, Integer shopId,
			Integer helpId, String idNo) throws Exception;
}
