package com.example.ProjectForSpringBoot.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.ProjectForSpringBoot.Service.IStaffService;
import java.util.List;

import com.example.ProjectForSpringBoot.CustomException.BusinessException;
import com.example.ProjectForSpringBoot.CustomException.ControllerException;
import com.example.ProjectForSpringBoot.Model.Help;
import com.example.ProjectForSpringBoot.Model.Staff;

@RestController
@RequestMapping("/staff")
public class StaffController {
	@Autowired
	IStaffService service;
	@GetMapping("/{pageNo}/{pageSize}")
	public ResponseEntity<?> getStaffBySearch(
			@PathVariable Integer pageNo,
			@PathVariable Integer pageSize,
			@RequestParam(required = false) String staffCode,
			@RequestParam(required = false) String staffName, 
			@RequestParam(required = false) String tel, 
			@RequestParam("status") String status, 
			@RequestParam(required = false) Integer shopId,
			@RequestParam(required = false) Integer helpId, 
			@RequestParam(required = false) String idNo) throws Exception{ 
		try {
			List<Staff> staff = service.getStaffLst(pageNo,pageSize,staffCode, staffName, tel, status, shopId, helpId, idNo);
			return new ResponseEntity<List<Staff>>(staff,HttpStatus.CREATED);
		} catch (BusinessException e) {			
			ControllerException ex = new ControllerException(e.getErrorMessage());
			return new ResponseEntity<ControllerException>(ex,HttpStatus.BAD_REQUEST);
		}
	}
}
