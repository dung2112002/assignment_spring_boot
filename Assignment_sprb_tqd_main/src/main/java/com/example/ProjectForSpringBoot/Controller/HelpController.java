package com.example.ProjectForSpringBoot.Controller;


import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.example.ProjectForSpringBoot.CustomException.BusinessException;
import com.example.ProjectForSpringBoot.CustomException.ControllerException;
import com.example.ProjectForSpringBoot.Model.Help;
import com.example.ProjectForSpringBoot.Service.HelpServiceIplm;
import com.example.ProjectForSpringBoot.Service.IHelpService;

@RestController
@RequestMapping("/help")
public class HelpController {

	@Autowired
	private IHelpService service;
	
	@GetMapping("/{pageNo}/{pageSize}")
	public ResponseEntity<?> helpList(@PathVariable Integer pageNo,@PathVariable Integer pageSize) throws BusinessException {
		try {
			List<Help> help = service.getHelpLst(pageNo,pageSize);
			return new ResponseEntity<List<Help>>(help,HttpStatus.CREATED);
		} catch (BusinessException e) {			
			ControllerException ex = new ControllerException(e.getErrorMessage());
			return new ResponseEntity<ControllerException>(ex,HttpStatus.BAD_REQUEST);
		}
	
	}
	@PostMapping("/insert")
	public ResponseEntity<?> insertHelp(@RequestBody Help helpInput){
		try {
			Help help = service.insertHelp(helpInput);
			return new ResponseEntity<Help>(help,HttpStatus.CREATED);
		} catch (BusinessException e) {
			ControllerException ex = new ControllerException(e.getErrorMessage());
			return new ResponseEntity<ControllerException>(ex,HttpStatus.BAD_REQUEST);
		}
			
		
	}
	@DeleteMapping("/delete/{helpId}")
	public ResponseEntity<?> deleteHelp(@PathVariable("helpId") Integer helpId) {	
		try {
			service.deleteHelp(helpId);
			return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
		} catch (Exception e) {
			BusinessException ex = new BusinessException();
			return new ResponseEntity<BusinessException>(ex,HttpStatus.BAD_REQUEST);
		}
			 		 
	}
	
	@PutMapping("/update")
	public ResponseEntity<?> updateHelp(@RequestParam("helpId")String helpId,@RequestBody Help helpRaw) {			
			try {
				Help help = service.updateHelp(helpId, helpRaw);
				return new ResponseEntity<Help>(help,HttpStatus.OK);
			} catch (BusinessException e) {
				ControllerException ex = new ControllerException(e.getErrorMessage());
				return new ResponseEntity<ControllerException>(ex,HttpStatus.BAD_REQUEST);
			}
		
	}
	@GetMapping("/search/{pageNo}/{pageSize}")
	public ResponseEntity<?> gethelpList (
			@PathVariable Integer pageNo,
			@PathVariable Integer pageSize,
			@RequestParam(required=false) Integer parentHelpId,
			@RequestParam(value="staffId") String staffId,			
			@RequestParam(required=false) String type,
			@RequestParam(required=false) String  status,
			@RequestParam(required=false) Integer  shopId) throws BusinessException{
	
		try {
			List<Help> help = service.getHelpLstBySearch(pageNo,pageSize,staffId, parentHelpId, status, type, shopId);
			return new ResponseEntity<List<Help>>(help,HttpStatus.CREATED);
		} catch (BusinessException e) {			
			ControllerException ex = new ControllerException(e.getErrorMessage());
			return new ResponseEntity<ControllerException>(ex,HttpStatus.BAD_REQUEST);
		}
		
	}
}
