package com.example.ProjectForSpringBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssignmentSprbTqdMainApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssignmentSprbTqdMainApplication.class, args);
	}

}
