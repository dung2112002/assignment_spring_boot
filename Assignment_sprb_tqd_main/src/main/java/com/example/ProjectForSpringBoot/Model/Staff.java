package com.example.ProjectForSpringBoot.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="staff")
public class Staff {
	@Id
	@GeneratedValue
	@Column(name="staff_id")
	private Integer staffId;
	public Staff(Integer staffId, String staffCode, String staffName, String tel, String address, Integer shopId,
			String status) {
		
		this.staffId = staffId;
		this.staffCode = staffCode;
		this.staffName = staffName;
		this.tel = tel;
		this.address = address;
		this.shopId = shopId;
		this.status = status;
	}
	public Integer getStaffId() {
		return staffId;
	}
	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Integer getShopId() {
		return shopId;
	}
	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Staff [staffId=" + staffId + ", staffCode=" + staffCode + ", staffName=" + staffName + ", tel=" + tel
				+ ", address=" + address + ", shopId=" + shopId + ", status=" + status + "]";
	}
	public Staff() {
		
	}
	@Column(name="staff_code",nullable = true)
	private String staffCode;
	@Column(name="staff_name",nullable = true)
	private String staffName;
	@Column(name="tel",nullable = true)
	private String tel;
	@Column(name="address",nullable = true)
	private String address;
	@Column(name="shop_id",nullable = true)
	private Integer shopId;
	@Column(name="id_no",nullable = true)
	private String id_no;
	public String getId_no() {
		return id_no;
	}
	public void setId_no(String id_no) {
		this.id_no = id_no;
	}
	@Column(name="status",nullable = true)
	private String status;
	
}
