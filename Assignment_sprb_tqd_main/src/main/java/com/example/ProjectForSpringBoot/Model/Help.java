package com.example.ProjectForSpringBoot.Model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="help")
public class Help {
	@Override
	public String toString() {
		return "Help [helpId=" + helpId + ", helpName=" + helpName + ", parentHelpId=" + parentHelpId + ", type=" + type
				+ ", position=" + position + ", content=" + content + ", status=" + status + ", createdUser="
				+ createdUser + "]";
	}
	@Id
	@GeneratedValue
	@Column(name="HELP_ID")
	private Integer helpId;
	@Column(name="HELP_NAME")
	private String helpName;
	@Column(name="PARENT_HELP_ID")
	private Integer parentHelpId;
	@Column(name="TYPE")
	private String type;
	@Column(name="POSITION")
	private String position;
	@Column(name="CONTENT")
	private String content;
	@Column(name="STATUS")
	private String status;
	@Column(name="CREATED_USER")
	private String createdUser;
	public Help(Integer helpId, String helpName, Integer parentHelpId, String type, String position, String content,
			String status, String createdUser) {
		this.helpId = helpId;
		this.helpName = helpName;
		this.parentHelpId = parentHelpId;
		this.type = type;
		this.position = position;
		this.content = content;
		this.status = status;
		this.createdUser = createdUser;
	}
	public Help() {}
	public Integer getHelpId() {
		return helpId;
	}
	public void setHelpId(Integer helpId) {
		this.helpId = helpId;
	}
	public String getHelpName() {
		return helpName;
	}
	public void setHelpName(String helpName) {
		this.helpName = helpName;
	}
	public Integer getParentHelpId() {
		return parentHelpId;
	}
	public void setParentHelpId(Integer parentHelpId) {
		this.parentHelpId = parentHelpId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	
}
