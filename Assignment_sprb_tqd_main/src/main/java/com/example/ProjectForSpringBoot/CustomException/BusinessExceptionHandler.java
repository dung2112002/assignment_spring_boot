package com.example.ProjectForSpringBoot.CustomException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class BusinessExceptionHandler {

	@ExceptionHandler(value= {BusinessException.class})
	public ResponseEntity<?> handleBusinessException(BusinessException e){
		BusinessException ex = new BusinessException(e.getErrorMessage());		
		return new ResponseEntity<BusinessException>(ex,HttpStatus.BAD_REQUEST);	
	}
}
